# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = Helene Hauge Kjerstad  
STUDENT ID = 111778

## Project description

The project is a train dispatch system written with java. 
The menu interface is text based and the user can choose between the following options:
* Add train departure
* Delete train departure
* Display train departures
* Display train departures by destination
* Display train departures by line
* Get train departure by id
* Update track for train departure
* Update delay for train departure
* Exit

## Project structure

All source files are stored in [src](./src). The [main/java/edu.ntnu.stud](./src/main/java/edu/ntnu/stud) package contains the main program structure. The package consist of the packages [launcher](./src/main/java/edu/ntnu/stud/launcher), [model](./src/main/java/edu/ntnu/stud/model) and [presentation](./src/main/java/edu/ntnu/stud/presentation). The [test/java/edu.ntnu.stud](./src/test/java/edu/ntnu/stud) package contains the tests for the model classes. The package consist of the package [model](./src/test/java/edu/ntnu/stud/model) that contains [TrainDepartureTest](./src/test/java/edu/ntnu/stud/model/TrainDepartureTest.java) and [TrainDepartureRegisterTest](./src/test/java/edu/ntnu/stud/model/TrainDepartureRegisterTest.java).

* [main/java/edu.ntnu.stud](./src/main/java/edu/ntnu/stud) contains the packages:
  * [launcher](./src/main/java/edu/ntnu/stud/launcher) contains the files: 
    * [TrainDispatchApp.java](./src/main/java/edu/ntnu/stud/launcher/TrainDispatchApp.java): Responsible for launching the application
  * [model](./src/main/java/edu/ntnu/stud/model) contains the files: 
    * [TrainDeparture.java](./src/main/java/edu/ntnu/stud/model/TrainDeparture.java): Represents a train departure
    * [TrainDepartureRegister.java](./src/main/java/edu/ntnu/stud/model/TrainDepartureRegister.java): Represents a register of train departures
  * [presentation](./src/main/java/edu/ntnu/stud/presentation) contains the files: 
    * [UserInterface.java](./src/main/java/edu/ntnu/stud/presentation/UserInterface.java): Responsible for the menu interface
    * [ValidateInput.java](./src/main/java/edu/ntnu/stud/presentation/ValidateInput.java): Responsible for reading validating user input
* [test/java/edu.ntnu.stud](./src/test/java/edu/ntnu/stud) contains the packages:
  * [model](./src/test/java/edu/ntnu/stud/model) contains the files: 
    * [TrainDepartureTest.java](./src/test/java/edu/ntnu/stud/model/TrainDepartureTest.java): Test class for TrainDeparture
    * [TrainDepartureRegisterTest.java](./src/test/java/edu/ntnu/stud/model/TrainDepartureRegisterTest.java): Test class for TrainDepartureRegister

## Link to repository

https://gitlab.stud.idi.ntnu.no/helenhkj/traindispatchsystem

## How to run the project

Before running the program:
* Make sure Java Development Kit (JDK) 17 is installed 
* Download and unzip the project folder

Run options:
* Run with IDE:
  * Open the [project folder](../TrainDispatchSystem) in your IDE of choice (need to be able to run java maven projects)
  * Run [TrainDispatchApp](./src/main/java/edu/ntnu/stud/launcher/TrainDispatchApp.java)

The [TrainDispatchApp](./src/main/java/edu/ntnu/stud/launcher/TrainDispatchApp.java) in the launcher package is the main class and contains the main method. When running the main method a text based menu should show up in the terminal. To choose a menu option enter the corresponding number. The program should run until the user exits.

## How to run the tests

Before running the tests:
* Make sure Java Development Kit (JDK) 17 is installed
* Download and unzip the project folder

Run options:
* Run with IDE:
  * Open the [project folder](../TrainDispatchSystem) in your IDE of choice (need to be able to run java maven projects)
  * Run [TrainDepartureTest](./src/test/java/edu/ntnu/stud/model/TrainDepartureTest.java)
  * Run [TrainDepartureRegisterTest](./src/test/java/edu/ntnu/stud/model/TrainDepartureRegisterTest.java)

## References

The test classes [TrainDepartureTest](./src/test/java/edu/ntnu/stud/model/TrainDepartureTest.java) and [TrainDepartureRegisterTest](./src/test/java/edu/ntnu/stud/model/TrainDepartureRegisterTest.java) are written with help from CoPilot.
