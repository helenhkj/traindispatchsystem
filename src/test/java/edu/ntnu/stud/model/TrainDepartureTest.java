package edu.ntnu.stud.model;

import java.time.Duration;
import java.time.LocalTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * This class contains tests for the TrainDeparture class.
 * <p>
 * Responsibilities: test the TrainDeparture class
 * Used CoPilot to help write the tests.
 * </p>
 *
 * @author Helene Kjerstad (10053)
 * @version 1.0
 */
public class TrainDepartureTest {

  /**
   * Positive tests for the TrainDeparture class.
   * <p>
   * does not throw exception when valid parameters are provided.
   * </p>
   *
   * @author Helene Kjerstad (10185)
   */
  @Nested
  @DisplayName("Positive tests, does not throw exception when valid parameters are provided")
  public class MethodsDoesNotThrowExceptions {
    private TrainDeparture trainDeparture;

    /**
     * Sets up method for the tests.
     * <p>
     * creates a new TrainDeparture object with valid parameters.
     * is run before each test.
     * </p>
     */
    @BeforeEach
    public void setup() {
      trainDeparture = new TrainDeparture(
          LocalTime.of(12, 0),
          "L1",
          123,
          "Trondheim",
          5);
    }

    @Test
    @DisplayName("Should not throw exception when valid parameters are provided")
    public void shouldNotThrowExceptionWhenValidParametersAreProvided() {
      try {
        new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.assertNotNull(trainDeparture);
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the constructor threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should get correct values when called")
    public void shouldGetCorrectValuesWhenCalled() {
      try {
        Assertions.assertEquals(LocalTime.of(12, 0), trainDeparture.getDepartureTime());
        Assertions.assertEquals("L1", trainDeparture.getLine());
        Assertions.assertEquals(123, trainDeparture.getTrainNumber());
        Assertions.assertEquals("Trondheim", trainDeparture.getDestination());
        Assertions.assertEquals(5, trainDeparture.getTrack());
        Assertions.assertEquals(Duration.ofHours(0).plusMinutes(0), trainDeparture.getDelay());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed with the exception message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should get departure time with delay when called")
    public void shouldGetDepartureTimeWithDelayWhenCalled() {
      try {
        trainDeparture.setDelay(Duration.ofHours(1).plusMinutes(10));
        Assertions.assertEquals(
            trainDeparture.getDepartureTime().plusHours(1).plusMinutes(10),
            trainDeparture.getDepartureTimeWithDelay());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed with the exception message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should set track when new track is positive integer")
    public void shouldSetTrackWhenNewTrackIsPositiveInteger() {
      try {
        trainDeparture.setTrack(2);
        Assertions.assertEquals(2, trainDeparture.getTrack());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the setTrack method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should set track when new track is -1")
    public void shouldSetTrackWhenNewTrackIsMinusOne() {
      try {
        trainDeparture.setTrack(-1);
        Assertions.assertEquals(-1, trainDeparture.getTrack());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the setTrack method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should set delay when new delay is not null")
    public void shouldSetDelayWhenNewDelayIsNotNull() {
      try {
        trainDeparture.setDelay(Duration.ofHours(1).plusMinutes(10));
        Assertions.assertEquals(Duration.ofHours(1).plusMinutes(10), trainDeparture.getDelay());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the setDelay method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should set delay when new delay is positive")
    public void shouldSetDelayWhenNewDelayIsPositive() {
      try {
        trainDeparture.setDelay(Duration.ofHours(1).plusMinutes(10));
        Assertions.assertEquals(Duration.ofHours(1).plusMinutes(10), trainDeparture.getDelay());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the setDelay method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return 0 when comparing two equal trainDeparture objects")
    public void shouldReturnZeroWhenComparingTwoEqualTrainDepartureObjects() {
      try {
        TrainDeparture trainDeparture2 = new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.assertEquals(0, trainDeparture.compareTo(trainDeparture2));
      } catch (Exception e) {
        Assertions.fail("The test failed because the compareTo method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return -1 when comparing two trainDeparture objects"
        + " where the first object is earlier than the second")
    public void shouldReturnMinusOneWhenTheFirstObjectIsEarlierThanTheSecond() {
      try {
        TrainDeparture trainDeparture2 = new TrainDeparture(
            LocalTime.of(12, 10),
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.assertEquals(-1, trainDeparture.compareTo(trainDeparture2));
      } catch (Exception e) {
        Assertions.fail("The test failed because the compareTo method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return 1 when comparing two trainDeparture objects"
        + " where the first object is later than the second")
    public void shouldReturnOneWhenTheFirstObjectIsLaterThanTheSecond() {
      try {
        TrainDeparture trainDeparture2 = new TrainDeparture(
            LocalTime.of(11, 50),
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.assertEquals(1, trainDeparture.compareTo(trainDeparture2));
      } catch (Exception e) {
        Assertions.fail("The test failed because the compareTo method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("ToString should return correct string")
    public void toStringShouldReturnCorrectString() {
      try {
        Assertions.assertEquals("""
         Departure time: 12:00
         Line: L1
         Train number: 123
         Destination: Trondheim
         Delay: \s
         Track: 5
            """, trainDeparture.toString());
      } catch (Exception e) {
        Assertions.fail("The test failed because the toString method threw an exception."
            + e.getMessage());
      }
    }
  }

  /**
   * Negative tests for the TrainDeparture class.
   * <p>
   * throws exception when invalid parameters are provided.
   * </p>
   *
   * @author Helene Kjerstad (10185)
   */
  @Nested
  @DisplayName("Negative tests, throws exception when invalid parameters are provided")
  public class MethodsThrowsExceptions {

    @Test
    @DisplayName("Constructor should throw exception when track is not a positive integer or -1")
    public void constructorShouldThrowExceptionWhenTrackIsNegativeExceptMinusOne() {
      try {
        new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            -5);
        Assertions.fail("The test failed because the constructor did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Track must be a positive integer or -1.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when trainNumber is not a positive integer")
    public void constructorShouldThrowExceptionWhenTrainNumberIsNegative() {
      try {
        new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            -123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Train number must be a positive integer.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when line is null")
    public void constructorShouldThrowExceptionWhenLineIsNull() {
      try {
        new TrainDeparture(
            LocalTime.of(12, 0),
            null,
            123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Line must be a non-empty string.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when line is empty")
    public void constructorShouldThrowExceptionWhenLineIsEmpty() {
      try {
        new TrainDeparture(
            LocalTime.of(12, 0),
            "",
            123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Line must be a non-empty string.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when destination is null")
    public void constructorShouldThrowExceptionWhenDestinationIsNull() {
      try {
        new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            null,
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Destination must be a non-empty string.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when destination is empty")
    public void constructorShouldThrowExceptionWhenDestinationIsEmpty() {
      try {
        new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Destination must be a non-empty string.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when departureTime is null")
    public void constructorShouldThrowExceptionWhenDepartureTimeIsNull() {
      try {
        new TrainDeparture(
            null,
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Departure time must be a LocalTime object.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Track setter should throw exception when track is not a positive integer or -1")
    public void trackSetterShouldThrowExceptionWhenTrackIsNegativeExceptMinusOne() {
      try {
        TrainDeparture trainDeparture = new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim");
        trainDeparture.setTrack(-5);

        Assertions.fail("The test failed because the setter did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Track must be a positive integer or -1.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Delay setter should throw exception when delay is null")
    public void delaySetterShouldThrowExceptionWhenDelayIsNull() {
      try {
        TrainDeparture trainDeparture = new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim");
        trainDeparture.setDelay(null);

        Assertions.fail("The test failed because the setter did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Delay must be a Duration object.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Delay setter should throw exception when delay is negative")
    public void delaySetterShouldThrowExceptionWhenDelayIsNegative() {
      try {
        TrainDeparture trainDeparture = new TrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim");
        trainDeparture.setDelay(Duration.ofHours(-1).plusMinutes(-10));

        Assertions.fail("The test failed because the setter did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Delay must be a positive Duration object.", e.getMessage());
      }
    }
  }
}
