package edu.ntnu.stud.model;

import java.time.Duration;
import java.time.LocalTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * This class contains tests for the TrainDepartureRegister class.
 * <p>
 * Responsibilities: test the TrainDepartureRegister class
 * </p>
 *
 * @author Helene Kjerstad (10185)
 */
public class TrainDepartureRegisterTest {

  private TrainDepartureRegister trainDepartureRegister;

  /**
   * Positive tests for the TrainDepartureRegister class.
   * <p>
   * does not throw exception when valid parameters are provided.
   * Used CoPilot to help write the tests.
   * </p>
   *
   * @author Helene Kjerstad (10053)
   * @version 1.0
   */
  @Nested
  @DisplayName("Positive tests, does not throw exception when valid parameters are provided")
  public class MethodsDoesNotThrowExceptions {

    /**
     * Sets up method for the tests.
     * <p>
     * creates a new TrainDepartureRegister object before each test.
     * is run before each test.
     * </p>
     */
    @BeforeEach
    public void setup() {
      trainDepartureRegister = new TrainDepartureRegister();
    }

    @Test
    @DisplayName("Should add train departure when all parameters are valid")
    public void shouldAddTrainDepartureWhenAllParametersAreValid() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim");
        Assertions.assertEquals(1, trainDepartureRegister.getNumberOfTrainDepartures());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should add train departure when all parameters are valid")
    public void shouldAddTrainDepartureWhenAllParametersAreValidWithTrack() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.assertEquals(1, trainDepartureRegister.getNumberOfTrainDepartures());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should get train departure when train departure exists")
    public void shouldGetTrainDepartureWhenTrainDepartureExists() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim");
        TrainDeparture trainDeparture = trainDepartureRegister.getTrainDeparture(123);
        Assertions.assertEquals(123, trainDeparture.getTrainNumber());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should remove train departure when train departure exists")
    public void shouldRemoveTrainDepartureWhenTrainDepartureExists() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim");
        trainDepartureRegister.removeTrainDeparture(123);
        Assertions.assertEquals(0, trainDepartureRegister.getNumberOfTrainDepartures());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return true if train number is unique")
    public void shouldReturnTrueIfTrainNumberIsUnique() {
      try {
        Assertions.assertTrue(trainDepartureRegister.isTrainNumberUnique(124));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return false if train number is not unique")
    public void shouldReturnFalseIfTrainNumberIsNotUnique() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            124,
            "Trondheim");
        Assertions.assertFalse(trainDepartureRegister.isTrainNumberUnique(124));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return true if track is available")
    public void shouldReturnTrueIfTrackIsAvailable() {
      try {
        Assertions.assertTrue(trainDepartureRegister.isTrackAvailable(
            5,
            LocalTime.of(12, 0)));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return false if track is not available")
    public void shouldReturnFalseIfTrackIsNotAvailable() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            124,
            "Trondheim",
            5);
        Assertions.assertFalse(trainDepartureRegister.isTrackAvailable(
            5,
            LocalTime.of(12, 0)));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return true if line is available")
    public void shouldReturnTrueIfLineIsAvailable() {
      try {
        Assertions.assertTrue(trainDepartureRegister.isLineAvailable(
            "L1",
            LocalTime.of(12, 0),
            "Trondheim"));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return false if line is not available")
    public void shouldReturnFalseIfLineIsNotAvailable() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            124,
            "Trondheim");
        Assertions.assertFalse(trainDepartureRegister.isLineAvailable(
            "L1",
            LocalTime.of(12, 0),
            "Trondheim"));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return true if time is before current time")
    public void shouldReturnTrueIfTimeIsBeforeCurrentTime() {
      try {
        trainDepartureRegister.setTime(LocalTime.of(12, 0));
        Assertions.assertTrue(trainDepartureRegister.isBefore(
            LocalTime.of(11, 0)));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return false if time is not before current time")
    public void shouldReturnFalseIfTimeIsNotBeforeCurrentTime() {
      try {
        trainDepartureRegister.setTime(LocalTime.of(12, 0));
        Assertions.assertFalse(trainDepartureRegister.isBefore(
            LocalTime.of(13, 0)));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should set track when track is not in use and all parameters are valid")
    public void shouldSetTrackWhenTrackIsNotInUse() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            124,
            "Trondheim");
        trainDepartureRegister.trainDepartureSetTrack(124, 5);
        Assertions.assertEquals(5, trainDepartureRegister.getTrainDeparture(124).getTrack());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should set track when track is -1")
    public void shouldSetTrackWhenTrackIsMinusOne() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            124,
            "Trondheim",
            5);
        trainDepartureRegister.trainDepartureSetTrack(124, -1);
        Assertions.assertEquals(-1, trainDepartureRegister.getTrainDeparture(124).getTrack());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should set delay when delay is not null and not negative")
    public void shouldSetDelayWhenAllParametersAreValid() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            124,
            "Trondheim");
        trainDepartureRegister.trainDepartureSetDelay(124, Duration.ofMinutes(5));
        Assertions.assertEquals(Duration.ofMinutes(5),
            trainDepartureRegister.getTrainDeparture(124).getDelay());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should set time when time is not null and after current time")
    public void shouldSetTimeWhenAllParametersAreValid() {
      try {
        trainDepartureRegister.setTime(LocalTime.of(12, 0));
        trainDepartureRegister.setTime(LocalTime.of(13, 0));
        Assertions.assertEquals(LocalTime.of(13, 0), trainDepartureRegister.getTime());
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because the method threw an exception."
            + e.getMessage());
      }
    }
  }

  /**
   * Negative tests for the TrainDepartureRegister class.
   * <p>
   * throws exception when invalid parameters are provided.
   * </p>
   *
   * @author Helene Kjerstad (10185)
   */
  @Nested
  @DisplayName("Negative tests, throws exception when invalid parameters are provided")
  public class MethodsThrowsExceptions {

    /**
     * Sets up method for the tests.
     * <p>
     * creates a new TrainDepartureRegister object before each test.
     * is run before each test.
     * </p>
     */
    @BeforeEach
    public void setup() {
      trainDepartureRegister = new TrainDepartureRegister();
    }

    @Test
    @DisplayName("Train departure getter should throw exception when train number does not exist")
    public void trainDepartureGetterShouldThrowExceptionWhenTrainDepartureDoesNotExist() {
      try {
        trainDepartureRegister.getTrainDeparture(1234);
        Assertions.fail("The test failed because the method did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to retrieve train departure with train number 1234.\n"
            + "Error: Train departure does not exist.", e.getMessage());
      }
    }

    @Test
    @DisplayName("RemoveTrainDeparture should throw exception when train number does not exist")
    public void removeTrainDepartureShouldThrowExceptionWhenTrainDepartureDoesNotExist() {
      try {
        trainDepartureRegister.removeTrainDeparture(1234);
        Assertions.fail("The test failed because the method did not throw"
            + " the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to remove train departure with train number 1234.\n"
            + "Error: Train departure does not exist.", e.getMessage());
      }
    }

    @Test
    @DisplayName("addTrainDeparture should throw exception when train number is not unique")
    public void addTrainDepartureShouldThrowExceptionWhenTrainNumberIsNotUnique() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            5);
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not add train departure.\nError: "
            + "Train number is not unique.", e.getMessage());
      }
    }

    @Test
    @DisplayName("addTrainDeparture should throw exception when track is already in use")
    public void addTrainDepartureShouldThrowExceptionWhenTrackIsAlreadyInUse() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            5);
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "R1",
            12,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not add train departure.\nError: "
            + "Track is already in use at the given time.", e.getMessage());
      }
    }

    @Test
    @DisplayName("addTrainDeparture should throw exception when line is not available")
    public void addTrainDepartureShouldThrowExceptionWhenLineIsNotAvailable() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            5);
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            12,
            "Trondheim");
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not add train departure.\n"
            + "Error: Another train with the same line and destination departs at the given time. "
            + "Line is not available.", e.getMessage());
      }
    }

    @Test
    @DisplayName("addTrainDeparture should throw exception when "
        + "departure time is before current time")
    public void addTrainDepartureShouldThrowExceptionWhenDepartureTimeIsBeforeCurrentTime() {
      try {
        trainDepartureRegister.setTime(LocalTime.of(12, 0));
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(10, 0),
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not add train departure.\nError: "
            + "Time must be after current time.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture should throw exception when "
        + "track is not a positive integer or -1")
    public void addTrainDepartureShouldThrowExceptionWhenTrackIsNegativeExceptMinusOne() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            -5);
        Assertions.fail("The test failed because the constructor did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Track must be a positive integer or -1.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture should throw exception when "
        + "trainNumber is not a positive integer")
    public void addTrainDepartureShouldThrowExceptionWhenTrainNumberIsNegative() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            -123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Train number must be a positive integer.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture should throw exception when line is null")
    public void addTrainDepartureShouldThrowExceptionWhenLineIsNull() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            null,
            123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Line must be a non-empty string.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture should throw exception when line is empty")
    public void addTrainDepartureShouldThrowExceptionWhenLineIsEmpty() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "",
            123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Line must be a non-empty string.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture should throw exception when destination is null")
    public void addTrainDepartureShouldThrowExceptionWhenDestinationIsNull() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            null,
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Destination must be a non-empty string.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture should throw exception when destination is empty")
    public void addTrainDepartureShouldThrowExceptionWhenDestinationIsEmpty() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Destination must be a non-empty string.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture should throw exception when departureTime is null")
    public void addTrainDepartureShouldThrowExceptionWhenDepartureTimeIsNull() {
      try {
        trainDepartureRegister.addTrainDeparture(
            null,
            "L1",
            123,
            "Trondheim",
            5);
        Assertions.fail("The test failed because the constructor did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Could not create instance of trainDeparture\n "
            + "Error: Departure time must be a LocalTime object.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Track setter should throw exception when track is negative except -1")
    public void trackSetterShouldThrowExceptionWhenTrackIsNegativeExceptMinusOne() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim");
        trainDepartureRegister.trainDepartureSetTrack(123, -5);
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Track must be a positive integer or -1.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Track setter should throw exception when track is in use")
    public void trackSetterShouldThrowExceptionWhenTrackIsInUse() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim",
            5);
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "R1",
            12,
            "Trondheim");
        trainDepartureRegister.trainDepartureSetTrack(12, 5);
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Track is already in use at the given time.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Track setter should throw exception when train number does not exist")
    public void trackSetterShouldThrowExceptionWhenTrainNumberDoesNotExist() {
      try {
        trainDepartureRegister.trainDepartureSetTrack(123, 5);
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to retrieve train departure with train number 123."
            + "\nError: Train departure does not exist.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Delay setter should throw exception when train number does not exist")
    public void delaySetterShouldThrowExceptionWhenTrainNumberDoesNotExist() {
      try {
        trainDepartureRegister.trainDepartureSetDelay(123, Duration.ofMinutes(5));
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to retrieve train departure with train number 123."
            + "\nError: Train departure does not exist.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Delay setter should throw exception when delay is null")
    public void delaySetterShouldThrowExceptionWhenDelayIsNull() {
      try {
        trainDepartureRegister.addTrainDeparture(
            LocalTime.of(12, 0),
            "L1",
            123,
            "Trondheim");
        trainDepartureRegister.trainDepartureSetDelay(123, null);
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Delay must be a Duration object.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Time setter should throw exception when time is null")
    public void timeSetterShouldThrowExceptionWhenTimeIsNull() {
      try {
        trainDepartureRegister.setTime(null);
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Time must be a LocalTime object.", e.getMessage());
      }
    }

    @Test
    @DisplayName("Time setter should throw exception when time is before current time")
    public void timeSetterShouldThrowExceptionWhenTimeIsBeforeCurrentTime() {
      try {
        trainDepartureRegister.setTime(LocalTime.of(12, 0));
        trainDepartureRegister.setTime(LocalTime.of(10, 0));
        Assertions.fail("The test failed because the method did not throw"
            + "the expected Ill.Arg.Exc.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Time must be after current time.", e.getMessage());
      }
    }
  }
}
