package edu.ntnu.stud.launcher;

import edu.ntnu.stud.presentation.UserInterface;

/**
 * This is the main class for the train dispatch application.
 * <p>
 * Responsibilities: launch the application
 * </p>
 *
 * @author Helene Kjerstad (10053)
 * @version 1.0
 */
public class TrainDispatchApp {

  /**
   * Main-method that runs the application.
   *
   * @param args are the arguments for the main-method.
   */
  public static void main(String[] args) {
    try {
      UserInterface ui = new UserInterface();
      ui.launch();
    } catch (Exception e) {
      System.out.println("Could not launch application \n Error: " + e.getMessage());
    }
  }
}
