package edu.ntnu.stud.model;

import java.time.Duration;
import java.time.LocalTime;

/**
 * This class represents a train departure.
 * <p>
 * Responsibilities: represent a train departure
 * </p>
 *
 * @author Helene Kjerstad (10053)
 * @version 1.0
 */
public class TrainDeparture implements Comparable<TrainDeparture> {
  // instance attributes
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private Duration delay;

  /**
   * Validates the track.
   *
   * @param track The track
   *
   * @throws IllegalArgumentException if track is not a positive integer or -1
   */
  private void validateTrack(int track) throws IllegalArgumentException {
    // helper method for constructor/setters, better cohesion
    if (!(track > 0 || track == -1)) { // -1 is used to indicate that the track is not set
      throw new IllegalArgumentException("Track must be a positive integer or -1.");
    }
  }

  /**
   * Validates the train number.
   *
   * @param trainNumber The train number
   *
   * @throws IllegalArgumentException if trainNumber is not a positive integer
   */
  private void validateTrainNumber(int trainNumber) throws IllegalArgumentException {
    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Train number must be a positive integer.");
    }
  }

  /**
   * Validates the departure time.
   *
   * @param departureTime The departure time
   *
   * @throws IllegalArgumentException if departureTime is null
   */
  private void validateDepartureTime(LocalTime departureTime) throws IllegalArgumentException {
    if (departureTime == null) {
      throw new IllegalArgumentException("Departure time must be a LocalTime object.");
    }
  }

  /**
   * Validates string parameters.
   *
   * @param string The string
   * @param parameterName The name of the string
   *
   * @throws IllegalArgumentException if string is null or an empty string
   */
  private void validateString(String string, String parameterName) throws IllegalArgumentException {
    if (string == null || string.isEmpty()) {
      throw new IllegalArgumentException(parameterName + " must be a non-empty string.");
    }
  }

  /**
   * Validates the delay.
   *
   * @param delay The delay
   *
   * @throws IllegalArgumentException if delay is null
   */
  private void validateDelay(Duration delay) throws IllegalArgumentException {
    if (delay == null) {
      throw new IllegalArgumentException("Delay must be a Duration object.");
    }
    if (delay.isNegative()) {
      throw new IllegalArgumentException("Delay must be a positive Duration object.");
    }
  }

  /**
   * Constructor for objects of class TrainDeparture.
   *
   * @param departureTime   The LocalTime object representing the departure time of
   *                        the trainDeparture
   * @param line            The identifier of the train line
   * @param trainNumber     The unique number of the train
   * @param destination     The destination of the train
   * @param track           The track number where the train will depart from.
   *                        Use -1 to indicate that the track is not set
   *
   * @throws IllegalArgumentException If:
   *                        <ul>
   *                          <li>departureTime is null</li>
   *                          <li>line is null or an empty string</li>
   *                          <li>trainNumber is not a positive integer</li>
   *                          <li>destination is null or an empty string</li>
   *                          <li>track is not a positive integer or -1</li>
   *                        </ul>
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber,
                        String destination, int track) throws IllegalArgumentException {
    try {
      validateTrack(track);
      validateTrainNumber(trainNumber);
      validateDepartureTime(departureTime);
      validateString(line, "Line");
      validateString(destination, "Destination");
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException(
          "Could not create instance of trainDeparture\n Error: " + e.getMessage());
    }

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = Duration.ofHours(0).plusMinutes(0);
  }

  /**
   * Constructor for objects of class TrainDeparture.
   *
   * @param departureTime   The LocalTime object representing the departure time of the train
   * @param line            The identifier of the train line
   * @param trainNumber     The unique number of the train
   * @param destination     The destination of the train
   *
   * @throws IllegalArgumentException If:
   *                        <ul>
   *                          <li>departureTime is null</li>
   *                          <li>line is null or an empty string</li>
   *                          <li>trainNumber is not a positive integer</li>
   *                          <li>destination is null or an empty string</li>
   *                          <li>track is not a positive integer or -1</li>
   *                        </ul>
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber, String destination)
      throws IllegalArgumentException {
    // calls the other constructor with track = -1
    this(departureTime, line, trainNumber, destination, -1);
  }

  /**
   * Retrieves the departure time of the train departure.
   *
   * @return The LocalTime object representing the departure time of the train
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Retrieves the identifier of the train line.
   *
   * @return The identifier of the train line
   */
  public String getLine() {
    return line;
  }

  /**
   * Retrieves the train number.
   *
   * @return The train number
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   *  Retrieves the destination.
   *
   * @return The destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Retrieves the track.
   * <p>
   * If the track is not set, -1 is returned.
   * </p>
   *
   * @return The track
   */
  public int getTrack() {
    return track;
  }

  /**
   * Retrieves the delay.
   *
   * @return The delay
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Retrieves the departure time with the delay added.
   *
   * @return The departure time with the delay added
   */
  public LocalTime getDepartureTimeWithDelay() {
    return getDepartureTime().plus(getDelay());
  }

  /**
   * Retrieves a string representation of the train departure.
   * <p>
   * The string representation is formatted from the given format string.
   * </p>
   *
   * @param format The format of the string representation
   *
   * @return The string representation of the train departure
   */
  public String getDisplayString(String format) {
    String delayString = LocalTime.MIDNIGHT.plus(delay).toString();
    String trackString = Integer.toString(track);

    if (delay.equals(Duration.ofHours(0).plusMinutes(0))) {
      delayString = " ";
    }
    if (track == -1) {
      trackString = " ";
    }

    return String.format(format,
        departureTime, line, trainNumber, destination, delayString, trackString);
  }

  /**
   * Sets the track.
   *
   * @param track The track
   *
   * @throws IllegalArgumentException if track is not a positive integer or -1.
   */
  public void setTrack(int track) throws IllegalArgumentException {
    validateTrack(track);
    this.track = track;
  }

  /**
   * Sets the delay.
   *
   * @param delay The delay
   *
   * @throws IllegalArgumentException if delay is null or negative
   */
  public void setDelay(Duration delay) throws IllegalArgumentException {
    validateDelay(delay);
    this.delay = delay;
  }

  /**
   * Retrieves a string representation of the train departure.
   *
   * @return The string representation of the train departure
   */
  @Override
  public String toString() {
    return getDisplayString("""
         Departure time: %s
         Line: %s
         Train number: %d
         Destination: %s
         Delay: %s
         Track: %s
            """);
  }

  /**
   * Compares this train departure with another train departure based on the departure time.
   * <p>
   * Used to sort train departures.
   * </p>
   *
   * @param other The other train departure
   *
   * @return     - 0 if the train departures are equal,
   *      <br>   - a negative integer if this train departure is before the other train departure,
   *      <br>   - a positive integer if this train departure is after the other train departure
   */
  @Override
  public int compareTo(TrainDeparture other) {
    return getDepartureTime().compareTo(other.getDepartureTime());
  }
}
