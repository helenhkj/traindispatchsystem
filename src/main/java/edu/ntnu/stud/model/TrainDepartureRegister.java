package edu.ntnu.stud.model;

import java.time.Duration;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.stream.Stream;

/**
 * This class represents a register with information about the train departures at a train station.
 * <p>
 * Responsibilities: Handle a collection of train departures.
 * </p>
 *
 * @author Helene Kjerstad (10053)
 * @version 1.0
 */
public class TrainDepartureRegister {
  private final HashMap<Integer, TrainDeparture> trainDepartures;
  private LocalTime time;

  /**
   * Validates the train number.
   *
   * @param trainNumber The train number
   *
   * @throws IllegalArgumentException if trainNumber is not unique
   */
  private void validateTrainNumber(int trainNumber) throws IllegalArgumentException {
    if (!isTrainNumberUnique(trainNumber)) {
      throw new IllegalArgumentException("Train number is not unique.");
    }
  }

  /**
   * Validates the track.
   *
   * @param track The track
   * @param departureTime The departure time
   *
   * @throws IllegalArgumentException if track is not available at the given time
   */
  private void validateAvailableTrack(int track, LocalTime departureTime)
      throws IllegalArgumentException {
    if (track < 0 && track != -1) {
      throw new IllegalArgumentException("Track must be a positive integer or -1.");
    }
    if (!isTrackAvailable(track, departureTime)) {
      throw new IllegalArgumentException("Track is already in use at the given time.");
    }
  }

  /**
   * Validates the line.
   *
   * @param line The line
   * @param departureTime The departure time
   * @param destination The destination
   *
   * @throws IllegalArgumentException if line is not available at the given time
   */
  private void validateAvailableLine(String line, LocalTime departureTime, String destination)
      throws IllegalArgumentException {
    if (!isLineAvailable(line, departureTime, destination)) {
      throw new IllegalArgumentException("Another train with the same line and destination"
          + " departs at the given time. Line is not available.");
    }
  }

  /**
   * Validates the train departure.
   *
   * @param trainNumber The train number
   *
   * @throws IllegalArgumentException if train departure does not exist
   */
  private void validateTrainDeparture(int trainNumber) throws IllegalArgumentException {
    if (isTrainNumberUnique(trainNumber)) {
      throw new IllegalArgumentException("Train departure does not exist.");
    }
  }

  /**
   * Validates the time.
   *
   * @param time The time
   *
   * @throws IllegalArgumentException if time is null or before the current time
   */
  private void validateTime(LocalTime time) throws IllegalArgumentException {
    if (time == null) {
      throw new IllegalArgumentException("Time must be a LocalTime object.");
    }
    if (time.isBefore(getTime())) {
      throw new IllegalArgumentException("Time must be after current time.");
    }
  }

  /**
   * Validates the new train departure in relation to the existing train departures.
   * <p>
   *   A new train departure is valid if:
   *   <ul>
   *     <li>trainNumber is unique</li>
   *     <li>line is available at the given time</li>
   *     <li>departureTime is after the current time</li>
   *   </ul>
   * </p>
   *
   * @param departureTime The departure time
   * @param line The line
   * @param trainNumber The train number
   * @param destination The destination
   *
   * @throws IllegalArgumentException if the new train departure is not valid
   */
  private void validateNewDeparture(LocalTime departureTime, String line, int trainNumber,
                                    String destination) throws IllegalArgumentException {
    validateTrainNumber(trainNumber);
    validateAvailableLine(line, departureTime, destination);
    validateTime(departureTime);
  }

  /**
   * Validates the new train departure in relation to the existing train departures.
   * <p>
   *   A new train departure is valid if:
   *   <ul>
   *     <li>trainNumber is unique</li>
   *     <li>line is available at the given time</li>
   *     <li>track is available at the given time</li>
   *     <li>departureTime is after the current time</li>
   *   </ul>
   * </p>
   *
   * @param departureTime The departure time
   * @param line The line
   * @param trainNumber The train number
   * @param destination The destination
   * @param track The track
   *
   * @throws IllegalArgumentException if the new train departure is not valid
   */
  private void validateNewDeparture(LocalTime departureTime, String line, int trainNumber,
                                    String destination, int track) throws IllegalArgumentException {
    validateNewDeparture(departureTime, line, trainNumber, destination);
    validateAvailableTrack(track, departureTime);
  }

  /**
   * Creates a sorted stream of the trainDepartures.
   *
   * @return The sorted stream of the trainDepartures
   */
  private Stream<TrainDeparture> sortedTrainDepartures() {
    return trainDepartures.values().stream().sorted();
  }

  /**
   * Creates a string representation of the trainDepartures.
   * <p>
   * Uses {@link #getFormatString()} to create a string representation of the table.
   * Displays the current time and the given train departures.
   * </p>
   *
   * @param departures The stream of train departures to display
   *
   * @return The string representation of the trainDepartures
   */
  private String createDisplayString(Stream<TrainDeparture> departures) {
    String tableFormat = getFormatString();
    String header = String.format(tableFormat,
        "Departure time", "Line", "Train number", "Destination", "Delay", "Track");
    int tableSpacing = header.length();
    String border = "\n" + "-".repeat(tableSpacing) + "\n";
    String clockFormat = "%" + (tableSpacing / 2) + "s" + "%-" + (tableSpacing / 2) + "s";
    String clock = String.format(clockFormat, "Clock: ", getTime());
    StringBuilder displayString = new StringBuilder();

    displayString.append(clock);
    displayString.append(border);
    displayString.append(header);
    displayString.append(border);

    if (departures != null) {
      departures.forEach(trainDeparture ->
          displayString.append(trainDeparture.getDisplayString(tableFormat)).append(border));
    }

    return displayString.toString();
  }

  /**
   * Creates a format string for the table.
   * <p>
   * Helper method for createDisplayString
   * </p>
   *
   * @return The format string for the table
   */
  private String getFormatString() {
    int departureTimeSpacing = 15;
    int lineSpacing = 8;
    int trainNumberSpacing = 15;
    int destinationSpacing = 15;
    int trackSpacing = 8;
    int delaySpacing = 15;
    String departureTimeFormat = "%-" + departureTimeSpacing + "." + departureTimeSpacing + "s";
    String lineFormat = "%-" + lineSpacing + "." + lineSpacing + "s";
    String trainNumberFormat = "%-" + trainNumberSpacing + "." + trainNumberSpacing + "s";
    String destinationFormat = "%-" + destinationSpacing + "." + destinationSpacing + "s";
    String delayFormat = "%-" + delaySpacing + "." + delaySpacing + "s";
    String trackFormat = "%-" + trackSpacing + "." + trackSpacing + "s";
    return String.format("| %s | %s | %s | %s | %s | %s |",
        departureTimeFormat, lineFormat, trainNumberFormat,
        destinationFormat, delayFormat, trackFormat);
  }

  /**
   * Constructor for objects of class TrainDepartureSystem.
   */
  public TrainDepartureRegister() {
    trainDepartures = new HashMap<>();
    time = LocalTime.MIDNIGHT;
  }

  /**
   * Retrieves the number of trainDepartures.
   *
   * @return The number of trainDepartures
   */
  public int getNumberOfTrainDepartures() {
    return trainDepartures.size();
  }

  /**
   * Retrieves the time.
   *
   * @return The time
   */
  public LocalTime getTime() {
    return time;
  }

  /**
   * Retrieves the trainDeparture with trainNumber.
   *
   * @return The trainDeparture with trainNumber
   */
  public TrainDeparture getTrainDeparture(int trainNumber) throws IllegalArgumentException {
    try {
      validateTrainDeparture(trainNumber);
      return trainDepartures.get(trainNumber);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to retrieve train departure with train number "
          + trainNumber + ".\nError: " + e.getMessage());
    }
  }

  /**
   * Retrieves a string representation of the trainDepartures
   * where the destination is the given station.
   * <p>
   * Uses {@link #createDisplayString(Stream)}
   * to create a string representation of the trainDepartures.
   * </p>
   *
   * @param station The station to filter by
   *
   * @return The string representation of the trainDepartures
   */
  public String getTrainDeparturesByStation(String station) {
    return createDisplayString(sortedTrainDepartures().filter(trainDeparture ->
        station.equalsIgnoreCase(trainDeparture.getDestination())));
  }

  /**
   * Retrieves a string representation of the trainDepartures
   * where the line is the given line.
   * <p>
   * Uses {@link #createDisplayString(Stream)}
   * to create a string representation of the trainDepartures.
   * </p>
   *
   * @param line The line to filter by
   *
   * @return The string representation of the trainDepartures
   */
  public String getTrainDeparturesByLine(String line) {
    return createDisplayString(sortedTrainDepartures().filter(trainDeparture ->
        line.equalsIgnoreCase(trainDeparture.getLine())));
  }

  /**
   * Retrieves a string representation of the trainDepartureSystem.
   * <p>
   * Uses {@link #createDisplayString(Stream)}
   * to create a string representation of the trainDepartureSystem.
   * </p>
   *
   * @return The string representation of the trainDepartureSystem
   */
  public String getDisplayString() {
    return createDisplayString(sortedTrainDepartures());
  }

  /**
   * Creates a train departure and adds it to the trainDepartures.
   * <p>
   *   Uses {@link #validateNewDeparture(LocalTime, String, int, String, int)}
   *   to validate the new train departure.
   * </p>
   *
   *
   * @param departureTime   The LocalTime object representing the departure time of the train
   * @param line            The identifier of the train line
   * @param trainNumber     The unique number of the train
   * @param destination     The destination of the train
   * @param track           The track number where the train will depart from
   *
   * @throws IllegalArgumentException If:
   *                           <ul>
   *                             <li>If trainNumber is not a positive integer</li>
   *                             <li>If departureTime is null</li>
   *                             <li>If line is null or an empty string</li>
   *                             <li>If destination is null or an empty string </li>
   *                             <li>If track is not a positive integer and not equal to -1</li>
   *                             <li>If line is not available at the given time</li>
   *                             <li>If track is not available at the given time</li>
   *                             <li>If departureTime is before the current time</li>
   *                           </ul>
   */
  public void addTrainDeparture(LocalTime departureTime, String line, int trainNumber,
                                String destination, int track) throws IllegalArgumentException {
    TrainDeparture trainDeparture =
        new TrainDeparture(departureTime, line, trainNumber, destination, track);
    try {
      validateNewDeparture(departureTime, line, trainNumber, destination, track);
      trainDepartures.put(trainDeparture.getTrainNumber(), trainDeparture);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Could not add train departure.\nError: "
          + e.getMessage());
    }
  }

  /**
   * Creates a train departure and adds it to the trainDepartures.
   * <p>
   * Uses {@link #validateNewDeparture(LocalTime, String, int, String)}
   * to validate the new train departure.
   * </p>
   *
   * @param departureTime   The LocalTime object representing the departure time of the train
   * @param line            The identifier of the train line
   * @param trainNumber     The unique number of the train
   * @param destination     The destination of the train
   *
   * @throws IllegalArgumentException If:
   *                           <ul>
   *                             <li>If trainNumber is not a positive integer</li>
   *                             <li>If departureTime is null</li>
   *                             <li>If line is null or an empty string</li>
   *                             <li>If destination is null or an empty string </li>
   *                             <li>If line is not available at the given time</li>
   *                             <li>If departureTime is before the current time</li>
   *                           </ul>
   */
  public void addTrainDeparture(LocalTime departureTime, String line, int trainNumber,
                                String destination) throws IllegalArgumentException {
    TrainDeparture trainDeparture =
        new TrainDeparture(departureTime, line, trainNumber, destination);
    try {
      validateNewDeparture(departureTime, line, trainNumber, destination);
      trainDepartures.put(trainDeparture.getTrainNumber(), trainDeparture);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Could not add train departure.\nError: "
          + e.getMessage());
    }
  }

  /**
   * Removes a trainDeparture from the trainDepartures with trainNumber.
   *
   * @param trainNumber The trainNumber of the trainDeparture to remove
   *
   * @throws IllegalArgumentException If the trainDeparture does not exist
   */
  public void removeTrainDeparture(int trainNumber) throws IllegalArgumentException {
    try {
      validateTrainDeparture(trainNumber);
      trainDepartures.remove(trainNumber);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to remove train departure with train number "
          + trainNumber + ".\nError: " + e.getMessage());
    }
  }

  /**
   * Retrieves a string representation of the trainDeparture with trainNumber.
   * <p>
   * Uses {@link #createDisplayString(Stream)}
   * to create a string representation of the trainDeparture.
   * </p>
   *
   * @param trainNumber The trainNumber of the trainDeparture to display
   *
   * @return The string representation of the trainDeparture
   */
  public String displayTrainDeparture(int trainNumber) throws IllegalArgumentException {
    return createDisplayString(Stream.of(getTrainDeparture(trainNumber)));
  }

  /**
   * Removes all trainDepartures that have departed.
   * <p>
   * A trainDeparture has departed if the departureTime is before the current time.
   * </p>
   */
  public void removeDepartedTrainDepartures() {
    trainDepartures.values().removeIf(trainDeparture ->
        isBefore(trainDeparture.getDepartureTimeWithDelay()));
  }

  /**
   * Checks if the trainNumber is unique in the register.
   *
   * @param trainNumber The trainNumber to check
   */
  public boolean isTrainNumberUnique(int trainNumber) {
    return !trainDepartures.containsKey(trainNumber);
  }

  /**
   * Checks if the track is available at the given time.
   *
   * @param track The track to check
   * @param departureTime The departure time
   *
   * @return true if the track is available at the given time, false otherwise
   */
  public boolean isTrackAvailable(int track, LocalTime departureTime) {
    if (track != -1) {
      boolean trackInUse = sortedTrainDepartures()
          .anyMatch(trainDeparture ->
              trainDeparture.getTrack() == track
                  && trainDeparture.getDepartureTimeWithDelay().equals(departureTime));

      return !trackInUse;
    }

    return true;
  }

  /**
   * Checks if the line is available at the given time.
   * <p>
   * A line is available if no other train with the same line and destination
   * departs at the given time.
   * </p>
   *
   * @param line The line to check
   * @param departureTime The departure time
   * @param destination The destination
   *
   * @return true if the line is available at the given time, false otherwise
   */
  public boolean isLineAvailable(String line, LocalTime departureTime, String destination) {
    boolean lineInUse = sortedTrainDepartures()
        .anyMatch(trainDeparture ->
            trainDeparture.getLine().equalsIgnoreCase(line)
                && trainDeparture.getDepartureTimeWithDelay().equals(departureTime)
                && trainDeparture.getDestination().equalsIgnoreCase(destination));

    return !lineInUse;
  }

  /**
   * Checks if the time is before the current time.
   * <p>
   * A time is before the current time if it is before or equal to the current time.
   * </p>
   *
   * @param time The time to check
   *
   * @return true if the time is before the current time, false otherwise
   */
  public boolean isBefore(LocalTime time) throws IllegalArgumentException {
    if (time == null) {
      throw new IllegalArgumentException("Time must be a LocalTime object.");
    }
    return time.isBefore(getTime()) || time.equals(getTime());
  }

  /**
   * Sets the time.
   * <p>
   * Uses {@link #validateTime(LocalTime)} to validate the time.
   * Calls {@link #removeDepartedTrainDepartures()}
   * to remove all trainDepartures that have departed.
   * </p>
   *
   * @param time The time to set
   *
   * @throws IllegalArgumentException if time is null or before the current time
   */
  public void setTime(LocalTime time) throws IllegalArgumentException {
    validateTime(time);
    this.time = time;
    removeDepartedTrainDepartures();
  }

  /**
   * Sets the track of the trainDeparture with trainNumber.
   * <p>
   * Uses {@link #validateAvailableTrack(int, LocalTime)} to validate the track.
   * </p>
   *
   * @param trainNumber The trainNumber of the trainDeparture to set track for
   * @param track       The track to set
   *
   * @throws IllegalArgumentException if track is not a positive integer and not equal to -1
   *                                  or if track is not available at the given time
   */
  public void trainDepartureSetTrack(int trainNumber, int track) throws IllegalArgumentException {
    validateAvailableTrack(track, getTrainDeparture(trainNumber).getDepartureTimeWithDelay());
    getTrainDeparture(trainNumber).setTrack(track);
  }

  /**
   * Sets the delay of the trainDeparture with trainNumber.
   *
   * @param trainNumber The trainNumber of the trainDeparture to set delay for
   * @param delay       The delay to set
   *
   * @throws IllegalArgumentException if delay is null or negative
   */
  public void trainDepartureSetDelay(int trainNumber, Duration delay)
      throws IllegalArgumentException {
    TrainDeparture trainDeparture = getTrainDeparture(trainNumber);
    trainDeparture.setDelay(delay);
    // set track to -1 if current track is not available at the delayed time
    if (isTrackAvailable(trainDeparture.getTrack(), trainDeparture.getDepartureTimeWithDelay())) {
      trainDeparture.setTrack(-1);
    }
    removeDepartedTrainDepartures();
  }
}
