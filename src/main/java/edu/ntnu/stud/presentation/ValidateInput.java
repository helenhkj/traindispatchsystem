package edu.ntnu.stud.presentation;

import edu.ntnu.stud.model.TrainDepartureRegister;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Scanner;

/**
 * Utility class with static methods for validating user input.
 * <p>
 * Uses the java.util.Scanner to receive user input from the terminal.
 * Responsibility: Read and validate user input.
 * </p>
 *
 * @author Helene Kjerstad (10053)
 * @version 1.0
 */
public final class ValidateInput {

  private static final Scanner SCANNER = new Scanner(System.in);

  /**
   * Private constructor to ensure that no objects of the class ValidateInput can be made by other
   * classes.
   */
  private ValidateInput() {}

  /**
   * Read an integer from user input.
   * <p>
   * Tries to parse the input to an integer, if it fails it will ask the user to try again.
   * Will continue to ask the user until it gets an integer.
   * </p>
   *
   * @param parameterName is a string describing the type of input expected
   *
   * @return the integer that the user has inputted
   */
  public static int inputInt(String parameterName) {
    while (true) {
      try {
        return Integer.parseInt(SCANNER.nextLine());
      } catch (NumberFormatException e) {
        System.out.printf("%s is not an integer, try again: ", parameterName);
      }
    }
  }

  /**
   * Read a time from user input.
   * <p>
   * Uses {@link #inputLocalTime()} to read the time from user input.
   * Checks that the time is after the current time in the train departure register.
   * </p>
   *
   * @param trainDepartureRegister is the train departure register
   * @param parameterName is a string describing the type of input expected
   *
   * @return the time that the user has inputted
   */
  public static LocalTime inputTime(
      TrainDepartureRegister trainDepartureRegister, String parameterName) {
    LocalTime time;
    System.out.printf("Enter %s (hh:mm): ", parameterName);
    while (true) {
      try {
        time = inputLocalTime();
        if (!trainDepartureRegister.isBefore(time)) {
          return time;
        } else {
          System.out.printf("%s must be after current time, try again: ", parameterName);
        }
      } catch (IllegalArgumentException e) {
        System.out.println(e.getMessage() + " try again: ");
      }
    }
  }

  /**
   * Read a LocalTime from user input.
   * <p>
   * Tries to parse the input to a time, if it fails it will ask the user to try again.
   * Will continue to ask the user until it gets a time.
   * </p>
   *
   * @return the LocalTime that the user has inputted
   */
  public static LocalTime inputLocalTime() {
    while (true) {
      try {
        return LocalTime.parse(SCANNER.nextLine());
      } catch (Exception e) {
        System.out.print("Input is not a time (hh:mm), try again: ");
      }
    }
  }

  /**
   * Read a duration from user input.
   * <p>
   * Uses {@link #inputLocalTime()} to read the LocalTime from user input.
   * Parses the LocalTime to a Duration.
   * </p>
   *
   * @param parameterName is a string describing the type of input expected
   *
   * @return the duration that the user has inputted
   */
  public static Duration inputDuration(String parameterName) {
    LocalTime time;
    System.out.printf("Enter %s (hh:mm): ", parameterName);
    while (true) {
      try {
        time = inputLocalTime();
        return Duration.ofHours(time.getHour()).plusMinutes(time.getMinute());
      } catch (Exception e) {
        System.out.printf(e.getMessage() + " try again: ");
      }
    }
  }

  /**
   * Read a string from user input.
   * <p>
   * Checks that the input is not empty. If it is empty it will ask the user to try again.
   * Will continue to ask the user until it gets a non-empty string.
   * </p>
   *
   * @param parameterName is a string describing the type of input expected
   *
   * @return the string that the user has inputted
   */
  public static String inputString(String parameterName) {
    System.out.printf("Enter %s: ", parameterName);
    while (true) {
      String input = SCANNER.nextLine();
      if (!input.isEmpty()) {
        return input;
      } else {
        System.out.printf("%s can not be empty, try again: ", parameterName);
      }
    }
  }

  /**
   * Read an integer from user input.
   * <p>
   * Uses {@link #inputInt(String)} to read the integer from user input.
   * Checks that the integer is in the specified range.
   * If it is not in the range it will ask the user to try again.
   * Will continue to ask the user until it gets an integer in the range.
   * </p>
   *
   * @param min is the minimum value of the integer
   * @param max is the maximum value of the integer
   * @param parameterName is a string describing the type of input expected
   *
   * @return the integer that the user has inputted
   */
  public static int inputIntInRange(int min, int max, String parameterName) {
    while (true) {
      int value = inputInt(parameterName);
      if (value >= min && value <= max) {
        return value;
      } else {
        System.out.printf("%s must be between %d and %d, try again: ", parameterName, min, max);
      }
    }
  }

  /**
   * Read a positive integer from user input.
   * <p>
   * Uses {@link #inputInt(String)} to read the integer from user input.
   * Checks that the integer is positive.
   * If it is not positive it will ask the user to try again.
   * Will continue to ask the user until it gets a positive integer.
   * </p>
   *
   * @param parameterName is a string describing the type of input expected
   *
   * @return the integer that the user has inputted
   */
  public static int inputPositiveInt(String parameterName) {
    while (true) {
      int value = inputInt(parameterName);
      if (value > 0) {
        return value;
      } else {
        System.out.printf("%s must be a positive integer, try again: ", parameterName);
      }
    }
  }

  /**
   * Read a train number from user input.
   * <p>
   * Uses {@link #inputPositiveInt(String)} to read the train number from user input.
   * Checks if the train number exists in the train departure register.
   * If it does not exist it will ask the user to try again.
   * </p>
   *
   * @param trainDepartureRegister is the train departure register
   *
   * @return the train number that the user has inputted
   *
   * @throws IllegalArgumentException if the user does not want to try again
   */
  public static int inputTrainNumber(TrainDepartureRegister trainDepartureRegister)
      throws IllegalArgumentException {
    while (true) {
      System.out.printf("Enter %s: ", "Train number");
      int trainNumber = inputPositiveInt("Train number");
      if (!trainDepartureRegister.isTrainNumberUnique(trainNumber)) {
        return trainNumber;
      } else {
        System.out.printf("Train number %d does not exist.\n", trainNumber);
        if (!promptRetry()) {
          throw new IllegalArgumentException("");
        }
      }
    }
  }

  /**
   * Read a train number from user input.
   * <p>
   * Uses {@link #inputPositiveInt(String)} to read the train number from user input.
   * Checks that the train number is unique.
   * If it is not unique it will ask the user to try again.
   * Will continue to ask the user until it gets a unique train number.
   * </p>
   *
   * @param trainDepartureRegister is the train departure register
   *
   * @return the train number that the user has inputted
   */
  public static int inputNewTrainNumber(TrainDepartureRegister trainDepartureRegister) {
    System.out.printf("Enter %s: ", "Train number");
    while (true) {
      int trainNumber = inputPositiveInt("Train number");
      if (trainDepartureRegister.isTrainNumberUnique(trainNumber)) {
        return trainNumber;
      } else {
        System.out.printf("Train number %d is already in use, try again: ", trainNumber);
      }
    }
  }

  /**
   * Read a track from user input.
   * <p>
   * Uses {@link #inputInt(String)} to read the track from user input.
   * Checks that the track is available at the specified departure time.
   * If it is not available it will ask the user to try again.
   * Will continue to ask the user until it gets an available track.
   * </p>
   *
   * @param trainDepartureRegister is the train departure register
   * @param departureTime is the departure time of the train
   *
   * @return the track that the user has inputted
   */
  public static int inputTrack(
      TrainDepartureRegister trainDepartureRegister, LocalTime departureTime) {
    System.out.printf("Enter %s: ", "Track");
    while (true) {
      int track = inputInt("Track");
      if (track <= 0 && track != -1) {
        System.out.print("Track must be a positive integer or -1, try again: ");
      } else if (trainDepartureRegister.isTrackAvailable(track, departureTime)) {
        return track;
      } else {
        System.out.printf("Track %d is not available at %s, try again\n", track, departureTime);
      }
    }
  }

  /**
   * Read a line from user input.
   * <p>
   * Uses {@link #inputString(String)} to read the line from user input.
   * Checks that the line is available at the specified departure time and destination.
   * If it is not available it will ask the user to try again.
   * Will continue to ask the user until it gets an available line.
   * </p>
   *
   * @param trainDepartureRegister is the train departure register
   * @param departureTime is the departure time of the train
   * @param destination is the destination of the train
   *
   * @return the line that the user has inputted
   */
  public static String inputLine(
      TrainDepartureRegister trainDepartureRegister, LocalTime departureTime, String destination) {
    while (true) {
      String line = inputString("Line");
      if (trainDepartureRegister.isLineAvailable(line, departureTime, destination)) {
        return line;
      } else {
        System.out.printf("Another train with line %s and destination %s departs at %s , "
            + "try again\n", line, destination, departureTime);
      }
    }
  }

  /**
   * Read a boolean from user input.
   * <p>
   * Uses {@link #inputString(String)} to read the boolean from user input.
   * Checks that the input is yes (y) or no (n).
   * If it is not yes or no it will ask the user to try again.
   * Will continue to ask the user until it gets yes or no.
   * </p>
   *
   * @return the boolean that the user has inputted
   */
  public static Boolean inputIsYes() {
    String input = SCANNER.nextLine();
    if (input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes")) {
      return true;
    } else if (input.equalsIgnoreCase("n") || input.equalsIgnoreCase("no")) {
      return false;
    } else {
      System.out.println("Input is not yes (y) or no (n), try again: ");
      return inputIsYes();
    }
  }

  /**
   * Ask the user if they want to try again.
   * <p>
   * Uses {@link #inputIsYes()} to ask the user if they want to try again.
   * </p>
   *
   * @return true if the user wants to try again, false otherwise
   */
  public static Boolean promptRetry() {
    System.out.println("Do you want to try again? [y/n]");
    return inputIsYes();
  }
}
