package edu.ntnu.stud.presentation;

import edu.ntnu.stud.model.TrainDepartureRegister;
import java.time.Duration;
import java.time.LocalTime;

/**
 * This class represents the user interface for the train dispatch application.
 * <p>
 * Responsibilities: Handle user interaction
 * </p>
 *
 * @author Helene Kjerstad (10053)
 * @version 1.0
 */
public class UserInterface {
  private TrainDepartureRegister trainDepartureRegister;

  // constants for the main menu
  private static final int DISPLAY_TRAIN_DEPARTURE_SYSTEM = 1;
  private static final int ADD_TRAIN_DEPARTURE = 2;
  private static final int REMOVE_TRAIN_DEPARTURE = 3;
  private static final int GET_TRAIN_DEPARTURE = 4;
  private static final int GET_TRAIN_DEPARTURES_BY_STATION = 5;
  private static final int GET_TRAIN_DEPARTURES_BY_LINE = 6;
  private static final int UPDATE_TRACK = 7;
  private static final int UPDATE_DELAY = 8;
  private static final int UPDATE_CLOCK = 9;
  private static final int EXIT = 0;

  /**
   * Launches the application.
   * <p>
   * Initializes the user interface and starts the main logic.
   * </p>
   */
  public void launch() {
    init();
    start();
  }

  /**
   * Main logic for the user interface.
   */
  private void start() {
    boolean run = true;
    while (run) {
      try {
        int option = mainMenu();
        switch (option) {
          case ADD_TRAIN_DEPARTURE -> addTrainDepartureFromInput();
          case REMOVE_TRAIN_DEPARTURE -> removeTrainDeparture();
          case DISPLAY_TRAIN_DEPARTURE_SYSTEM ->  displayTrainDepartureSystem();
          case UPDATE_CLOCK -> updateTime();
          case GET_TRAIN_DEPARTURE -> displayTrainDeparture();
          case GET_TRAIN_DEPARTURES_BY_STATION -> displayTrainDeparturesByStation();
          case GET_TRAIN_DEPARTURES_BY_LINE -> displayTrainDeparturesByLine();
          case UPDATE_TRACK -> updateTrack();
          case UPDATE_DELAY -> updateDelay();
          case EXIT -> run = false;
          default -> System.out.println("Invalid option");
        }
      } catch (Exception e) {
        System.out.println("Application failed with error: " + e.getMessage());
        System.out.println("Do you want to start again? [y/n]");
        if (ValidateInput.inputIsYes()) {
          init();
        } else {
          run = false;
        }
      }
    }
    System.out.println("Exiting application...");
  }

  /**
   * Initializes the user interface.
   */
  private void init() {
    trainDepartureRegister = new TrainDepartureRegister();
    addTestData();
  }

  /**
   * Displays the train departure system.
   */
  private void displayTrainDepartureSystem() {
    System.out.println(trainDepartureRegister.getDisplayString());
  }

  /**
   * Updates the time of the train departure system from user input.
   * <p>
   * The user is prompted to input a new time until a valid time is provided.
   * </p>
   */
  private void updateTime() {
    LocalTime time;
    while (true) {
      try {
        time = ValidateInput.inputTime(trainDepartureRegister, "New time");
        trainDepartureRegister.setTime(time);
        break;
      } catch (IllegalArgumentException e) {
        System.out.println(e.getMessage());
        if (!ValidateInput.promptRetry()) {
          break;
        }
      }
    }
  }

  /**
   * Adds a train departure to the train departure system from user input.
   * <p>
   * The user is prompted to input a new train departure until a valid train departure is provided.
   * Then the train departure is added to the train departure register.
   * </p>
   */
  private void addTrainDepartureFromInput() {
    boolean success = false;
    LocalTime departureTime;
    String line;
    String destination;
    int trainNumber;
    int track;

    while (!success) {
      trainNumber = ValidateInput.inputNewTrainNumber(trainDepartureRegister);
      departureTime = ValidateInput.inputTime(trainDepartureRegister, "Departure time");
      destination = ValidateInput.inputString("Destination");
      line = ValidateInput.inputLine(trainDepartureRegister, departureTime, destination);

      System.out.println("Do you want add track? [y/n] ");
      if (ValidateInput.inputIsYes()) {
        track = ValidateInput.inputTrack(trainDepartureRegister, departureTime);
        success = addTrainDeparture(departureTime, line, trainNumber, destination, track);
      } else {
        success = addTrainDeparture(departureTime, line, trainNumber, destination);
      }

      if (!success) {
        if (!ValidateInput.promptRetry()) {
          success = true;
        }
      }
    }
  }

  /**
   * Adds a train departure to the train departure register.
   * <p>
   * Returns true if the train departure was added and false if an exception was thrown.
   * </p>
   *
   * @param departureTime The departure time
   * @param line          The line
   * @param trainNumber   The train number
   * @param destination   The destination
   *
   * @return true if the train departure was added, false otherwise
   */
  private Boolean addTrainDeparture(LocalTime departureTime, String line, int trainNumber,
                                 String destination) {
    try {
      trainDepartureRegister.addTrainDeparture(departureTime, line, trainNumber, destination);
      return true;
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      return false;
    }
  }

  /**
   * Adds a train departure to the train departure register.
   * <p>
   * Returns true if the train departure was added and false if an exception was thrown.
   * </p>
   *
   * @param departureTime The departure time
   * @param line          The line
   * @param trainNumber   The train number
   * @param destination   The destination
   * @param track         The track
   *
   * @return true if the train departure was added, false otherwise
   */
  private Boolean addTrainDeparture(LocalTime departureTime, String line, int trainNumber,
                                 String destination, int track) {
    try {
      trainDepartureRegister.addTrainDeparture(
          departureTime, line, trainNumber, destination, track);
      return true;
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      return false;
    }
  }

  /**
   * Removes a train departure from the train departure system by train number.
   * <p>
   * The user is prompted to input a train number until a valid train number is provided.
   * Then the train departure is removed from the train departure register.
   * </p>
   */
  private void removeTrainDeparture() {
    try {
      int trainNumber = ValidateInput.inputTrainNumber(trainDepartureRegister);
      trainDepartureRegister.removeTrainDeparture(trainNumber);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Displays train departure from train departure system by train number.
   * <p>
   * The user is prompted to input a train number until a valid train number is provided.
   * Then the train departure is displayed from the train departure register.
   * </p>
   */
  private void displayTrainDeparture() {
    try {
      int trainNumber = ValidateInput.inputTrainNumber(trainDepartureRegister);
      System.out.println(trainDepartureRegister.displayTrainDeparture(trainNumber));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Displays train departures from train departure system by station.
   */
  private void displayTrainDeparturesByStation() {
    String station = ValidateInput.inputString("Station");
    try {
      System.out.println(trainDepartureRegister.getTrainDeparturesByStation(station));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Displays train departures from train departure system by line.
   */
  private void displayTrainDeparturesByLine() {
    String line = ValidateInput.inputString("Line");
    try {
      System.out.println(trainDepartureRegister.getTrainDeparturesByLine(line));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Updates the track for a train departure from user input.
   * <p>
   * The user is prompted to input a train number until a valid train number is provided.
   * Then the user is prompted to input a track until a valid track is provided.
   * Then the track is updated for the train departure.
   * </p>
   */
  private void updateTrack() {
    try {
      int trainNumber = ValidateInput.inputTrainNumber(trainDepartureRegister);
      int track = ValidateInput.inputTrack(trainDepartureRegister,
          trainDepartureRegister.getTrainDeparture(trainNumber).getDepartureTime());
      trainDepartureRegister.trainDepartureSetTrack(trainNumber, track);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Updates the delay for a train departure from user input.
   * <p>
   * The user is prompted to input a train number until a valid train number is provided.
   * Then the user is prompted to input a delay until a valid delay is provided.
   * Then the delay is updated for the train departure.
   * </p>
   */
  private void updateDelay() {
    try {
      int trainNumber = ValidateInput.inputTrainNumber(trainDepartureRegister);
      Duration delay = ValidateInput.inputDuration("Delay");
      trainDepartureRegister.trainDepartureSetDelay(trainNumber, delay);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Displays the main menu and returns the user's choice.
   * <p>
   * The user is prompted to input a choice until a valid choice is provided.
   * Then the choice is returned.
   * </p>
   *
   * @return The user's choice
   */
  private int mainMenu() {
    System.out.println("\nTrain Dispatch System");
    System.out.printf("[%d] Display train departure system\n", DISPLAY_TRAIN_DEPARTURE_SYSTEM);
    System.out.printf("[%d] Add train departure\n", ADD_TRAIN_DEPARTURE);
    System.out.printf("[%d] Remove train departure\n", REMOVE_TRAIN_DEPARTURE);
    System.out.printf("[%d] Get train departure\n", GET_TRAIN_DEPARTURE);
    System.out.printf("[%d] Get train departures by station\n", GET_TRAIN_DEPARTURES_BY_STATION);
    System.out.printf("[%d] Get train departures by line\n", GET_TRAIN_DEPARTURES_BY_LINE);
    System.out.printf("[%d] Update the track for a train departure \n", UPDATE_TRACK);
    System.out.printf("[%d] Update the delay for a train departure \n", UPDATE_DELAY);
    System.out.printf("[%d] Update clock \n", UPDATE_CLOCK);
    System.out.printf("[%d] Exit\n", EXIT);
    System.out.print("Enter choice: ");
    return ValidateInput.inputIntInRange(EXIT, UPDATE_CLOCK, "Choice");
  }

  /**
   * Adds test data to the train departure register.
   */
  private void addTestData() {
    try {
      trainDepartureRegister.addTrainDeparture(LocalTime.of(10, 0), "L1", 123, "Trondheim", 5);
      trainDepartureRegister.addTrainDeparture(LocalTime.of(10, 10), "L2", 450, "Lillehammer");
      trainDepartureRegister.addTrainDeparture(LocalTime.of(10, 20), "RE1", 654, "Oslo", 1);
      trainDepartureRegister.addTrainDeparture(LocalTime.of(10, 30), "B1", 632, "Trondheim");
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }
}
